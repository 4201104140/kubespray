getent group

 ~/.ssh/config
 ~/.ssh/id_rsa
 ssh -F ~/.ssh/config 61.28.238.126
 ssh-keygen -t rsa -b 4096

 ansible all -m ping -i inventory/mycluster/hosts.yml 
 ansible-playbook -i inventory/mycluster/hosts.yml cluster.yml
 ansible-playbook -i inventory/mycluster/hosts.yaml reset.yml

VENVDIR=kubespray-venv
KUBESPRAYDIR=kubespray
cd ..
source $VENVDIR/bin/activate
cd $KUBESPRAYDIR

 all:
  hosts:
    node1:
      ansible_host: 61.28.238.126
      ip: 192.168.0.3
      access_ip: 192.168.0.3
  children:
    kube_control_plane:
      hosts:
        node1:
    kube_node:
      hosts:
        node1:
    etcd:
      hosts:
        node1:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}
